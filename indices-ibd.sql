﻿-- criar a tabela pessoa
drop table if exists pessoa;
create table pessoa (
 id serial, 
 nome varchar(30),
 sobrenome varchar(30),
 endereco varchar(60),
 constraint pessoa_id_pk primary key(id));

-- a criação de índices é útil quando há um grande volume de dados a ser armazenado!!
-- criando um índice para nome e sobrenome
create index idx_nome_sobrenome_pessoa on pessoa using btree(nome,sobrenome); -- o algoritmo é opcional

create index idx_endereco on pessoa(endereco);

--há a opção de criação de índices cujo valores são únicos
create unique index idx_nome_endereco on pessoa (nome,endereco);

-- o btree é o algoritmo padrão, mas existem outras opções, tal como o hash.

--inserindo valores em pessoas
insert into pessoa(nome,sobrenome,endereco) values('Rodrigo','Santos','Rua-1'),
('Erik','Silva','Rua-2'),
('Carla','Santana','Rua-3'); 

-- selecionando os valores
select nome, sobrenome, endereco
from pessoa

insert into pessoa(nome,sobrenome,endereco) values('Erik','Xavier','Rua-2'); -- O que acontece agora??
-- Definimos um indice único para o par de atributos nome e endereço e, por coincidência, o nome Erik e o endereço Rua-2
-- já existem. Assim não é possível inserir valores. Um índice também é uma restrição.

-- Para verificar a ação dos indices nas consultas anterior vamos explicar a consulta.

Explain
select nome, sobrenome, endereco
from pessoa
-- A procura foi feita sem acionar índice algum
-- Agora

Explain
select nome, sobrenome, endereco
from pessoa
where nome = 'Erik'
-- Observem o resultado da consulta 

-- drop index 
-- apagando o índice 
drop index idx_nome_endereco; -- o nome do índice tem que ser único.

-- Observação: uma chave primária é um índice único. Para provar veja a aplicação da consulta na tabela de sistema 
SELECT 
    tablename, 
    indexname, 
    indexdef 
FROM 
    pg_indexes 
WHERE 
    tablename = 'pessoa';
--OK

-- É possivel criar uma tabela sem chave primária?
create table pagamento(
  id int,
  descricao varchar(60),
  valor numeric(8,3));
  
-- Para indicar que o campo id é único pode-se recorrer a um índice
create unique index idx_id_pagamento on pagamento using btree(id desc); --- lembrar de colocar o algoritmo, se for o caso,
-- entre a declaração da tabela e dos campos dos índices. Cada campo pode usar asc ou desc (ascendente ou descendente).

-- Vamos criar um índice descendente para a descricao 
create index idx_nome_pagamento on pagamento (descricao desc);



   