﻿-- VIEWS (Visões) são consultas armazenadas que mapeiam objetos. Views são usadas inclusive por questões de segurança. Assim voce
-- pode deixar uma consulta pública, porém o acesso as tabelas de origem podem ser mais restritas.

-- Exemplo
create table cliente (
  id serial,
  nome varchar(30),
  primary key (id));

create table compra (
  id serial,
  data date,
  numero varchar(6),
  clienteid int,
  primary key(id),
  foreign key (clienteid) references cliente);

create table itens_compra (
  id serial,
  descricao varchar(30),
  valor numeric(10,2),
  compraid int,
  primary key (id) foreign key (compraid) references compra);	
  
-- inserindo valores
insert into cliente values(1,'Erika'),(2,'Carlos'),(3,'Cinthia');
insert into compra values(1,'10-06-2019','000001',1),(2,'05-06-2019','000002',2),(3,'01-04-2019','000003',3);
insert into itens_compra values(1,'Arroz',18.90,1),(2,'Feijão',5.60,1);
insert into itens_compra values(3,'Arroz',17.60,2),(4,'Abacate',6.00,2),(5,'Abobora',7.00,2);

-- Podemos saber sobre todas essas compras por meio de uma view
create or replace view compras_cliente as
select clie.nome, comp.numero, comp.data, item.descricao, item.valor
from cliente clie inner join compra comp on (clie.id = comp.clienteid)
inner join itens_compra item on (item.compraid = comp.id)  

-- A vantagem é que você só precisa fazer a view uma única vez
-- Para usar basta fazer uma consulta a própria view

select * from compras_cliente

-- podemos renomear normalmente os campos de saída da view
create or replace view compras_cliente as
select clie.nome, comp.numero, comp.data, item.descricao, item.valor
from cliente clie inner join compra comp on (clie.id = comp.clienteid)
inner join itens_compra item on (item.compraid = comp.id)  
